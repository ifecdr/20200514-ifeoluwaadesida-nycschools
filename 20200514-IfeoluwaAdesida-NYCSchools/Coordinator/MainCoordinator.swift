//
//  MainCoordinator.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    var childCoordinator = [Coordinator]()
    var navController: UINavigationController
    var schoolListViewModel: SchoolListViewModel
    var schoolDetailsViewModel: SchoolDetailsViewModel
    
    init(navigationController: UINavigationController,
         schoolListVM: SchoolListViewModel = SchoolListViewModel(),
         schoolDetailVM: SchoolDetailsViewModel = SchoolDetailsViewModel()) {
        self.navController = navigationController
        self.schoolListViewModel = schoolListVM
        self.schoolDetailsViewModel = schoolDetailVM
    }
    
    func start() {
        let vc = SchoolListViewController.instantiate()
        vc.viewModel = schoolListViewModel
        vc.coordinator = self
        navController.pushViewController(vc, animated: false)
    }
    
    func goToDetails(for school: School) {
        let vc = SchoolDetailsViewController.instantiate()
        schoolDetailsViewModel.school = school
        vc.viewModel = schoolDetailsViewModel
        vc.coordinator = self
        navController.pushViewController(vc, animated: true)
    }
}
