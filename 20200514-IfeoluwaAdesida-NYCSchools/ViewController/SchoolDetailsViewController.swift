//
//  SchoolDetailsViewController.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController, Storyboarded {
    @IBOutlet weak var schoolNameLbl: UILabel!
    @IBOutlet weak var schoolWebsiteLbl: UILabel!
    @IBOutlet weak var schoolAddressLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var zipLbl: UILabel!
    @IBOutlet weak var testTakersLbl: UILabel!
    @IBOutlet weak var mathAverageLbl: UILabel!
    @IBOutlet weak var readingAverageLbl: UILabel!
    @IBOutlet weak var writingAverageLbl: UILabel!
    
    var viewModel: SchoolDetailsViewModel!
    weak var coordinator: MainCoordinator?

    var satResult: SatResult! {
        didSet {
            setSatResult()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSatResult()
        config()
        self.title = "School Detail"
    }
    
    // Get selected school sat result
    private func getSatResult() {
        viewModel.getSatResult { result in
            switch result {
            case .success(let satResult):
                self.satResult = satResult
            case .failure(let err):
                self.showError()
                print(err.localizedDescription)
            }
        }
    }
    
    private func showError() {
        DispatchQueue.main.async {
            let msg = "Sat Test record was not found"
            let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // config element UIs with school details
    private func config() {
        let school = viewModel.school
        schoolNameLbl.text = school?.schoolName
        schoolWebsiteLbl.text = school?.website
        schoolAddressLbl.text = school?.primaryAddress
        cityLbl.text = school?.city
        stateLbl.text = school?.stateCode
        zipLbl.text = school?.zip
    }
    
    // config control text with sat result for the school
    private func setSatResult() {
        DispatchQueue.main.async {
            self.testTakersLbl.text = self.satResult.numOfSatTestTakers
            self.mathAverageLbl.text = self.satResult.satMathAvgScore
            self.readingAverageLbl.text = self.satResult.satCriticalReadingAvgScore
            self.writingAverageLbl.text = self.satResult.satWritingAvgScore
        }
    }
}
