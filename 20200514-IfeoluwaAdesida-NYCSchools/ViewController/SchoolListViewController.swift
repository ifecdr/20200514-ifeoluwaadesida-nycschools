//
//  SchoolListViewController.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController, Storyboarded {

    @IBOutlet weak var tableView: UITableView!
    var viewModel: SchoolListViewModel!
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getSchool()
        viewModel.delegate = self
        self.title = "NYC School List"
    }
    
    // Display the Selected School Detail and Sat Result
    private func showDetailVC(school: School) {
        coordinator?.goToDetails(for: school)
    }
}

extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getSchoolListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolListTableViewCell.identifier,
                                                       for: indexPath) as? SchoolListTableViewCell else {
            fatalError("Unable to create tableview Cell")
        }
        
        cell.config(viewModel.schoolList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.schoolList[indexPath.row]
        showDetailVC(school: school)
    }
}

extension SchoolListViewController: SchollListViewModelDelegate{
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
