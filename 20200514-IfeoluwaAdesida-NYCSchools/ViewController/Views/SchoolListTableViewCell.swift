//
//  SchoolListTableViewCell.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {

    @IBOutlet weak var classNameLbl: UILabel!
    static let identifier = "schoolCell"
    
    func config(_ school: School) {
        classNameLbl.text = school.schoolName
    }
}
