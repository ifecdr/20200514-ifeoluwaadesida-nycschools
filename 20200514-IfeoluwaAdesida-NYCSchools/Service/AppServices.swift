//
//  AppServices.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import Foundation

protocol ServiceObject {
    func getSchool(completion: @escaping (Result<[School], Error>) -> Void)
    func getSatResult(_ dbc: String, completion: @escaping (Result<SatResult, Error>) -> Void)
}

final class AppServices: ServiceObject {
    //single instance of class
    static let shared = AppServices()
    
    // create session allowing caching
    private let session: URLSession
    
    // private initializer
    private init() {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = false
        config.requestCachePolicy = .returnCacheDataElseLoad
        session = URLSession.init(configuration: config)
    }
    
    /// Get the list all schools in NYC
    ///
    /// Completion - (Result<[School], Error>)
    func getSchool(completion: @escaping (Result<[School], Error>) -> Void) {
        guard let url = URL(string: ConstantUtils.schoolUrl) else {
            completion(.failure(NSError(domain: "Bad Url", code: 1, userInfo: nil)))
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        session.dataTask(with: request) { (data, _, error) in
            if let err = error {
                completion(.failure(err))
            }
            
            if let result = data {
                do {                    
                    let response = try JSONDecoder().decode([School].self, from: result)
                    completion(.success(response))
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(.failure(error))
                }
            }
        }.resume()
    }
    
    /// Get School Sat Result
    ///
    /// param: School dbc
    /// completion: (Result<SatResult, Error>)
    func getSatResult(_ dbn: String, completion: @escaping (Result<SatResult, Error>) -> Void) {
        let param = "?dbn=\(dbn)"
        
        guard let url = URL(string: ConstantUtils.satUrl + param) else {
            completion(.failure(NSError(domain: "Bad Url", code: 1, userInfo: nil)))
            return
        }

        let request = URLRequest(url: url)
        
        session.dataTask(with: request) { (data, _, error) in
            if let err = error {
                completion(.failure(err))
            }
            
            if let result = data {
                do {
                    let response = try JSONDecoder().decode([SatResult].self, from: result)
                    if !response.isEmpty {
                        completion(.success(response[0]))
                    } else {
                        completion(.failure(NSError(domain: "No Response", code: 1, userInfo: nil)))
                    }
                    
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(.failure(error))
                }
            }
        }.resume()
    
    }
}
