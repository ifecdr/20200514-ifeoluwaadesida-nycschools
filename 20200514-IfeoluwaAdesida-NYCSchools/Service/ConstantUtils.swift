//
//  ConstantUtils.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import Foundation

class ConstantUtils {
    static let schoolUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2"
    static let satUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4"
}
