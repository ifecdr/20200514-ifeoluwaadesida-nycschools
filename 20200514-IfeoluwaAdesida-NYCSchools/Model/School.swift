//
//  School.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import Foundation

struct School: Decodable {
    let dbn: String
    let schoolName: String
    let overview: String
    let website: String
    let totalStudents: String
    let primaryAddress: String
    let city: String
    let zip: String
    let stateCode: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overview = "overview_paragraph"
        case website
        case totalStudents = "total_students"
        case primaryAddress = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
    }
    
    public init (from decoder: Decoder) throws {
        let value = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try value.decode(String.self, forKey: .dbn)
        schoolName = try value.decode(String.self, forKey: .schoolName)
        overview = try value.decode(String.self, forKey: .overview)
        website = try value.decode(String.self, forKey: .website)
        totalStudents = try value.decode(String.self, forKey: .totalStudents)
        primaryAddress = try value.decode(String.self, forKey: .primaryAddress)
        city = try value.decode(String.self, forKey: .city)
        zip = try value.decode(String.self, forKey: .zip)
        stateCode = try value.decode(String.self, forKey: .stateCode)
    }
}
