//
//  SchoolDetailsViewModel.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import Foundation

class SchoolDetailsViewModel {
    var serviceObject: ServiceObject?
    var school: School?
        
    init(serviceObject: ServiceObject = AppServices.shared) {
        self.serviceObject = serviceObject
    }
    
    /// Get School Sat Result
    func getSatResult(_ completion: @escaping (Result<SatResult, Error>) -> Void) {
        guard let dbn = school?.dbn else {
            return
        }
        serviceObject?.getSatResult(dbn, completion: completion)
    }
}
