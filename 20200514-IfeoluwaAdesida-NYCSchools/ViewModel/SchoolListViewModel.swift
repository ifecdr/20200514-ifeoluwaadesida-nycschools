//
//  SchoolListViewModel.swift
//  20200514-IfeoluwaAdesida-NYCSchools
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import Foundation

protocol SchollListViewModelDelegate: class {
    func reloadData()
}

class SchoolListViewModel {
    var schoolList: [School] = [School]() {
        didSet {
            delegate?.reloadData()
        }
    }
    weak var delegate: SchollListViewModelDelegate?
    var serviceObject: ServiceObject?
    
    init(serviceObject: ServiceObject = AppServices.shared) {
        self.serviceObject = serviceObject
    }
    
    /// Get Schools List Count
    func getSchoolListCount() -> Int? {
        return schoolList.count
    }
    
    /// Get School In NYC
    func getSchool() {
        serviceObject?.getSchool(completion: { result in
            switch result {
            case .success(let schools):
                self.schoolList = schools
            case .failure(let err):
                print(err.localizedDescription)
            }
        })
    }
}
