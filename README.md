# NYC School application by Ifeoluwa Adesida

Building and Running Application
Clone or download this repo and run on xcode.
There are no third party pods included in the projet file and wouldn't need any pod install.
# Overview
This application has two views.
The first displays list of school consumed from this endpoint
https://data.cityofnewyork.us/resource/s3k6-pzi2.json
The second view is a detail view for selected school. This view also displays other detail
information about the selected school and also a sat result if available from this endpoint
https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=01M292 passing a dbn string
parameter...

# Design Pattern
This project makes use of MVVM+C