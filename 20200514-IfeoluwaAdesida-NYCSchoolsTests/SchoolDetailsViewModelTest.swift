//
//  SchoolDetailsViewModelTest.swift
//  20200514-IfeoluwaAdesida-NYCSchoolsTests
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import XCTest
@testable import _0200514_IfeoluwaAdesida_NYCSchools

class SchoolDetailsViewModelTest: XCTestCase {

    var sut: SchoolDetailsViewModel!
    override func setUp() {
        let serviceObject = MockAppService()
        let expectation = self.expectation(description: "get school")
        serviceObject.getSchool { result in
            switch result {
            case .success(let sch):
                self.sut = SchoolDetailsViewModel(serviceObject: serviceObject)
                self.sut.school = sch[140]
                expectation.fulfill()
            case .failure(let err):
                XCTFail(err.localizedDescription)
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }

    override func tearDown() {
        sut = nil
    }

    func testGetSatResult() {
        var satResult: SatResult!
        let expectation = self.expectation(description: "get sat result")
        sut.getSatResult { result in
            switch result {
            case .success(let result):
                satResult = result
                expectation.fulfill()
            case .failure(let err):
                XCTFail(err.localizedDescription)
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(satResult.dbn, "01M292")
        XCTAssertEqual(satResult.numOfSatTestTakers, "29")
        XCTAssertEqual(satResult.satCriticalReadingAvgScore, "355")
        XCTAssertEqual(satResult.satMathAvgScore, "404")
        XCTAssertEqual(satResult.satWritingAvgScore, "363")
    }
}
