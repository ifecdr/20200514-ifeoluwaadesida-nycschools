//
//  SchoolListViewModelTest.swift
//  20200514-IfeoluwaAdesida-NYCSchoolsTests
//
//  Created by MAC Consultant on 5/14/20.
//  Copyright © 2020 Ife. All rights reserved.
//

import XCTest
@testable import _0200514_IfeoluwaAdesida_NYCSchools

class SchoolListViewModelTest: XCTestCase {

    var sut: SchoolListViewModel!
    override func setUp() {
        let serviceObject = MockAppService()
        sut = SchoolListViewModel(serviceObject: serviceObject)
    }

    override func tearDown() {
        sut = nil
    }

    func testGetSchool() {
        sut.getSchool()
        sleep(5)
        XCTAssertNotNil(sut.schoolList)
        XCTAssertEqual(sut.schoolList[0].dbn, "02M260")
        XCTAssertEqual(sut.schoolList[0].totalStudents, "376")
    }

    func testGetSchoolListCount() {
        testGetSchool()
        let schoolsCount = sut.getSchoolListCount()
        XCTAssertEqual(schoolsCount, 440)
    }

}

class MockAppService: ServiceObject {
    func getSchool(completion: @escaping (Result<[School], Error>) -> Void) {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "SchoolList", withExtension: "json") else {
            XCTFail("Missing file: SchoolList.json")
            return
        }
        
        do {
            let json = try Data(contentsOf: url)
            let respObject = try JSONDecoder().decode([School].self, from: json)
            completion(.success(respObject))
        } catch {
            completion(.failure(error))
        }
    }
    
    func getSatResult(_ dbc: String, completion: @escaping (Result<SatResult, Error>) -> Void) {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "SatResult", withExtension: "json") else {
            XCTFail("Missing file: SatResult.json")
            return
        }
        
        do {
            let json = try Data(contentsOf: url)
            let respObject = try JSONDecoder().decode([SatResult].self, from: json)
            completion(.success(respObject[0]))
        } catch {
            completion(.failure(error))
        }
    }
}

